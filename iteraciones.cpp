#include <iostream>

using namespace std;

int main(){
	int h;

cout << "Ciclo Para (for)\n\t";
	for(h=0;h<10;h++){
		cout << h <<" ";
	}
cout<<endl;
cout << "Ciclo Mientras (while)\n\t";
	h = 0;
	while(h<10){
		cout << h <<" ";
		h++;
	}
cout<<endl;
cout << "ciclo hacer..Mientras (do...While)\n\t";
	h = 15;
	do{
		cout << h <<" ";
		h++;
	}while(h<10);
cout<<endl;
return 0;
}



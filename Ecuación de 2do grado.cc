/********************************************************/
/*Nombre: Ejercicio 2 de la Guía 1                      */
/*Autor: Axel Díaz                                      */
/*Correo: diaz.axelio@gmail.com                         */
/*Fecha: 15/10/2010                                     */
/********************************************************/

/******************** ENUNCIADO *************************/
/*H acer un programa que calcule                        */
/* la ecuación de 2º grado                              */
/********************************************************/
#include <iostream>
#include <stdlib.h>
#include <math.h>
using namespace std;

main(){
	double x, a, b, c;
	char Resp;
	
	do{
		system("reset");
	cout<<"A continuación ingresará 3 números.\n";
	cout<<"\nIngrese el primer número: "; cin>>a;
	cout<<"\nIngrese el segundo número: "; cin>>b;
	cout<<"\nIngrese el tercer número: "; cin>>c;
	while(c<=0){
		cout<<"ERROR! Este número no puede ser 0 ni negativo, por favor introduzca otro número: ";
		cin>>c;
	}
	
	x = sqrt(pow(a+b, 4))/pow(5*b, 1/2);
	
	cout<<"\nEl resultado es: "<<x;
	
	cout<<"\n\n\nDesea repetir el proceso? (S/N)";
	cin>>Resp;
	}while(Resp=='s'||Resp=='S');

}

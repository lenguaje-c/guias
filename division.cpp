#include <iostream>
#include <math.h>
using namespace std;

int main(){
	int a,b,c,f;
	float x,y;
	
	cout<<"Introduzca el valor de A ";
	cin>>a;
	cout<<"Introduzca el valor de B ";
	cin>>b;
	cout<<"Introduzca el valor de C ";
	cin>>c;
	
	f = ((2*c)+b);
	
	if(f == 0){
		cout << "Error 1: Division por 0"<<endl;
	}else{
		y = ((3*a)/(f));
		if(y < 0){
			cout <<"Error 2: Raiz de Numero Negativo"<<endl;
		}else{
			x = sqrt(y);
			cout << "El resultado de x es "<<x<<endl;
		}	
	}

return 0;
}



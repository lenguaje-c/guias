/********************************************************/
/*Nombre: Ejercicio 1 de la Guía 1                      */
/*Autor: Axel Díaz                                      */
/*Correo: diaz.axelio@gmail.com                         */
/*Fecha: 15/10/2010                                     */
/********************************************************/

/***********************ENUNCIADO************************/
/*1. Realice un programa en C++ que:                    */
/*a.Se le ingresen dos numero (a y b).                  */
/*b. Si los dos números son iguales, los sume.          */
/*c. Si son diferentes, reste el mayor menos el menor.  */
/*d. Muestre el resultado de la operación.              */
/********************************************************/
 
#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;
main(){
	int num1, num2;
	char Resp;
	do{
		system("reset");
	cout<<"Ingrese el primer número: ";
	cin>>num1;
	cout<<"\nAhora ingrese el segundo número: ";
	cin>>num2;
	
	if(num1==num2){
		cout<<"\n\nComo los dos números son iguales, al sumarse el resultado es: "<<num1+num2;
	}else if(num1>num2){
		cout<<"Como el primer número es mayor, al restarse con "<<num2<<" da como resultado: "<<num1-num2;
	}else if(num2>num1){
		cout<<"Como el segundo número es mayor, al restarse con "<<num1<<" da como resultado: "<<num2-num1;
	}
	
	cout<<"\n\n\nDeseas repetir el proceso? (S/N)";
	cin>>Resp;
	}while(Resp=='s'||Resp=='S');
}

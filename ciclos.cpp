#include <iostream>
using namespace std;

int main(){

	int x;
	
	cout << "Ciclo para (for):\n\t";
	for(x = 5; x > 0; x--){
		cout << x <<" ";
	}
	cout << endl;
	
	cout<<"Ciclo Mientras (while)\n\t";
	x=10;
	while(x<=5){
		cout << x <<" ";
		x++;
	}
	cout<<endl;
	
	cout<<"Ciclo hacer...\"Mientras\"(do..While)\n\t";
	x=10;
	do{
		cout<<x<<" ";
		x++;
	}while(x<5);
	cout<<endl;

return 0;
}


//	  arod.unerg@gmail.com

//	  www.estudiosweb.com.ve






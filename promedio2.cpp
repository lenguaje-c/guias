#include <iostream>
#define CANT 3

using namespace std;

int main(){
	int calif, i, acum=0;
	float prom;
	
for(i=0; i<CANT; i++){
	do{
		cout << "Introduzca una nota valida ";
		cin >> calif;
	}while(calif < 1 or calif >10);
	
	acum += calif;
}

prom = (float)acum / CANT;

cout << "Promedio :"<<prom<<endl;

if(prom >= 5.5){
	cout << "Aprobado";
}else{
	cout << "Reprobado";
}
cout<<endl;
return 0;
}

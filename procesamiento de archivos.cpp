//***********************************************
//  archiv01.cpp
//  Demuestra la escritura b�sica en archivo
//  �1999, Jaime Virgilio G�mez Negrete
//***********************************************

#include <fstream.h>
#include <iostream>
using namespace std;
int main()
{
    ofstream archivo;  // objeto de la clase ofstream

    archivo.open("Saludo.txt");

    archivo << "Primera l�nea de texto" << endl;
    archivo << "Segunda l�nea de texto" << endl;
    archivo << "�ltima l�nea de texto" << endl;

    archivo.close();
    return 0;
} 

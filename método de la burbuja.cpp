#include<iostream>
#include<stdlib.h>
#include<windows.h>


#define CANT 10
#define NUM 9
using namespace std;

void mostrar(int *v){
int i;
	for(i=0; i<CANT; i++){
		cout<<v[i]<<" ";
	}
}

void pantalla(int *v, int *d, int j){
    int i;
	system("cls");			
	cout<<"Vector Desorganizado:\n\t";
	mostrar(v);
	cout<<endl;

	cout<<"\nOrganizacion:\n\t";
	mostrar(d);
	cout<<"\n\t";
	for(i=0; i<j; i++){
       cout<<"  ";
    }
    if(j>=0){cout<<"^-^";}
	cout<<endl;
    Sleep(300);
//	wait((double)1000);
}

void llenar(int *v){
int i;
srand(time(NULL));
	for(i=0; i<CANT; i++){
		v[i] = 1+rand()%NUM;
	}
}

void copiar(int *vO, int *vC){
	int i;
	for(i=0; i<CANT; i++){
		vC[i] = vO[i];
	}
}

int main(){

int vector[CANT];
int Vaux[CANT];
int i,j,aux,aux1,n;

llenar(vector);
copiar(vector,Vaux);

pantalla(vector, Vaux, -1);

cout<<"Presione una tecla para empezar a Organizar: ";
cin.get();

cout<<endl;
//------ Metodo de la Burbuja ------//
for(i=0; i<CANT;i++){
	for(j=0; j<CANT-1; j++){
        pantalla(vector, Vaux, j);
		if(Vaux[j] > Vaux[j+1]){

			aux = Vaux[j];
			Vaux[j] = Vaux[j+1];
			Vaux[j+1] = aux;

        }
        pantalla(vector, Vaux, j);
	}
}

system("pause");
return 0;
}

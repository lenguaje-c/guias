//A un trabajador le descuentan de su sueldo el 10% si su sueldo es menor o igual a 1000, por
//encima de 1000 y hasta 2000 el 5% del adicional, y por encima de 2000 el 3% del adicional. Calcular el
//descuento y sueldo neto que recibe el trabajador dado su sueldo.

#include <iostream>
using namespace std;

int main() {
    float sueldo, adicional, descuento, descuentos, descuento1, descuento2, descuento3, adicional1, adicional2;
    
    cout<<"Por favor introduzca su sueldo actual"<<endl;
    cin>>sueldo;
    
    
    descuento=sueldo*10/100;
    descuento1=1000*10/100;
    descuento2=1000*5/100;
    descuento3=1000*3/100;
    adicional1=sueldo-1000;
    adicional2=sueldo*2000;
        
    if(sueldo<=1000){
                     sueldo=sueldo-descuento;
                     cout<<"El descuento es "<<descuento<<" y el sueldo es "<<sueldo;
                     }else{
                           if(2000>=sueldo>1000){
                                                 descuentos=descuento1-descuento2;
                                                 sueldo=sueldo-descuentos;
                                                 cout<<"El descuento es "<<descuentos<<" y el sueldo es "<<sueldo;
                                                 }else{
                                                       if(sueldo>2000){
                                                                       descuentos=descuento1-descuento2-descuento3;
                                                                       sueldo=sueldo-descuentos;
                                                                       cout<<"El descuento es "<<descuentos<<" y el sueldo es "<<sueldo;
                                                                       }
                                                                       }
                                                                       }
    
    cin.get();cin.get();
    return 0;
}

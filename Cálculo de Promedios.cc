/********************************************************/
/*Nombre: Ejercicio 2 de la Guía 1                      */
/*Autor: Axel Díaz                                      */
/*Correo: diaz.axelio@gmail.com                         */
/*Fecha: 18/10/2010                                     */
/********************************************************/

/********************ENUNCIADO***************************/
/*Realice un programa en C++ que calcule el promedio    */
/*de 10 notas, e identifique si el alumno se encuentra  */
/*reprobado o aprobado, (nota entre 1 y 10 puntos,      */
/*nota mínima aprobatoria 6 puntos).                    */
/********************************************************/

#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;
main(){
char Resp;
int i=0, nota;
float prom;
do{
	prom=0;
	system("reset");
	srand(time(NULL));
	nota=1 + rand() % 10;
	prom+=nota;
	cout<<"\nPrimera nota: "<<nota;
	nota=1 + rand() % 10;
	prom+=nota;
	cout<<"\nSegunda nota: "<<nota;
	nota=1 + rand() % 10;
	prom+=nota;
	cout<<"\nTercera nota: "<<nota;
	nota=1 + rand() % 10;
	prom+=nota;
	cout<<"\nCuarta nota: "<<nota;
	nota=1 + rand() % 10;
	prom+=nota;
	cout<<"\nQuinta nota: "<<nota;
	nota=1 + rand() % 10;
	prom+=nota;
	cout<<"\nSexta nota: "<<nota;
	nota=1 + rand() % 10;
	prom+=nota;
	cout<<"\nSéptima nota: "<<nota;
	nota=1 + rand() % 10;
	prom+=nota;
	cout<<"\nOctava nota: "<<nota;
	nota=1 + rand() % 10;
	prom+=nota;
	cout<<"\nNovena nota: "<<nota;
	nota=1 + rand() % 10;
	prom+=nota;
	cout<<"\nDécima nota: "<<nota;
	nota=1 + rand() % 10;
	prom+=nota;
	
	cout<<"\n\nPor lo tanto el promedio es: "<<prom/10;
	if((prom/10)>=6){cout<<"\nY por eso, el alumno está aprobado";}
	else{cout<<"\nY por eso, el alumno está reprobado";}
	
cout<<"\n\nDesea repetir el proceso? (S/N) ";
cin>>Resp;
}while(Resp=='s'||Resp=='S');
}
